# Randomizer
_Takes a series of items and returns a single item._

## Usage
Input should be a series of items as plain text separated by line.


```
itemA
itemB
itemC
```

Randomization will prefer items towards the top of the list. Upon reaching the end of the input, a result will be printed.

Randomization performed by [Random.org](https://www.random.org).
